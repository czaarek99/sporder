# Sporder #

An alternative spotify client

### Commit Guidelines ###
* [IMPROVED] - For things that were not broken but now work smoother and better
* [ADDED] - For new features
* [FIXED] - For bugfixes
* [REMOVED] - For removed features
* [LIBRARY] - For dependency modifications
* [WIP] - For improvements and additions of not yet working features
* [CHANGED] - For changes that don't really affect the application in neither a negative or positive way
